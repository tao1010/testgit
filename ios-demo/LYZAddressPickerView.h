//
//  LYZAddressPickerView.h
//  LeYiZhuang
//
//  Created by dengtao on 2018/8/10.
//  Copyright © 2018年 leyizhuang. All rights reserved.
//

#import <UIKit/UIKit.h>

#define FirstComponent 0
#define SubComponent 1
#define ThirdComponent 2

@interface LYZAddressPickerView : UIView<UIPickerViewDataSource,UIPickerViewDelegate>
{
    NSInteger _count1;
    NSInteger _count2;
    NSInteger _count3;
}
@property(nonatomic,strong)NSDictionary* dict;
@property(nonatomic,strong)NSArray* pickerArray;
@property(nonatomic,strong)NSArray* subPickerArray;
@property(nonatomic,strong)NSArray* thirdPickerArray;
@property(nonatomic,strong)NSArray* selectArray;

@property(nonatomic,strong) void (^handleActionWithAddressView)(NSMutableDictionary *proviceDic,NSMutableDictionary *cityDic,NSMutableDictionary *countyDic);

//设置默认选择省市区
- (void)selectAddressWithProvice:(NSString *)provice;
- (void)selectAddressWithCity:(NSString *)city;
- (void)selectAddressWithCountry:(NSString *)county;

@end
