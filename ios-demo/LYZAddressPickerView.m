//
//  LYZAddressPickerView.m
//  LeYiZhuang
//
//  Created by dengtao on 2018/8/10.
//  Copyright © 2018年 leyizhuang. All rights reserved.
//

#import "LYZAddressPickerView.h"
#import "FMDB.h"
//http://blog.cocoachina.com/article/25083
#import <JavaScriptCore/JavaScriptCore.h>

@interface LYZAddressPickerView(){
    
    FMDatabase *_db;
    NSString *_dbPath;
}
@property(nonatomic, assign) BOOL isFormJS;
@property(nonatomic, strong) NSDictionary *areaDic;

@end

@implementation LYZAddressPickerView

@synthesize dict=_dict;
@synthesize pickerArray=_pickerArray;
@synthesize subPickerArray=_subPickerArray;
@synthesize thirdPickerArray=_thirdPickerArray;
@synthesize selectArray=_selectArray;

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIPickerView * pickView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 216)];
        pickView.delegate = self;
        pickView.dataSource = self;
        pickView.backgroundColor = kColorBackGround;
        [self addSubview:pickView];
        
        self.isFormJS = YES;
//        self.isFormJS = NO;
        if (self.isFormJS) {
            
            //从本地JS获取数据
            NSString *path = [[NSBundle mainBundle] pathForResource:@"city.data-3" ofType:@"js"];
            NSString *scriptAreaString = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
            //将JS 的字符串转换为数组
            JSContext *jsContext = [[JSContext alloc] init];
            JSValue *localJS = [jsContext evaluateScript:scriptAreaString];
            JSValue *chinaAreaArray=[jsContext evaluateScript:@"cityData3"];
            NSLog(@"names :%@",[chinaAreaArray toArray]);
            
            
//            NSArray *chinaArray = [_dbPath[@"cityData3"]]
            
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            
            
            
//                [dic setValue:[rs stringForColumn:@"area_name"] forKey:@"area_name"];
//                [dic setValue:[rs stringForColumn:@"area_parent"] forKey:@"area_parent"];
//                    //省没有地区编码Code 不存在时，dic 无key:area_code
//                [dic setValue:[rs stringForColumn:@"area_code"] forKey:@"area_code"];
//                [dic setValue:[rs stringForColumn:@"area_id"] forKey:@"area_id"];
//
//                [array addObject:dic];
//            }
//            self.pickerArray = array;
//            
//            NSMutableArray *array = [[NSMutableArray alloc] init];
//            while ([rs next]) {
//                NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
//                [dic setValue:[rs stringForColumn:@"area_name"] forKey:@"area_name"];
//                [dic setValue:[rs stringForColumn:@"area_parent"] forKey:@"area_parent"];
//                [dic setValue:[rs stringForColumn:@"area_code"] forKey:@"area_code"];
//                [dic setValue:[rs stringForColumn:@"area_id"] forKey:@"area_id"];
//
//                    //        NSLog(@"%@",[rs stringForColumn:@"area_name"]);
//                    //        NSLog(@"%@",[rs stringForColumn:@"area_parent"]);
//                    //        NSLog(@"%@",[rs stringForColumn:@"area_id"]);
//
//                [array addObject:dic];
//            }
//            self.subPickerArray = array;
//
//            NSMutableArray *array = [[NSMutableArray alloc] init];
//            while ([rs next]) {
//                NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
//                [dic setValue:[rs stringForColumn:@"area_name"] forKey:@"area_name"];
//                [dic setValue:[rs stringForColumn:@"area_parent"] forKey:@"area_parent"];
//                [dic setValue:[rs stringForColumn:@"area_code"] forKey:@"area_code"];
//                [dic setValue:[rs stringForColumn:@"area_id"] forKey:@"area_id"];
//
//                    //        NSLog(@"%@",[rs stringForColumn:@"area_name"]);
//                    //        NSLog(@"%@",[rs stringForColumn:@"area_parent"]);
//                    //        NSLog(@"%@",[rs stringForColumn:@"area_id"]);
//
//                [array addObject:dic];
//            }
//            self.thirdPickerArray = array;
            
            
            
        }else{
            
                //从area.db获取地址数据
            NSString *docsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
            _dbPath   = [docsPath stringByAppendingPathComponent:@"area.db"];
            
            NSFileManager *fm = [NSFileManager defaultManager];
            BOOL isExist = [fm fileExistsAtPath:_dbPath];
            
            if (!isExist)
            {
                    //拷贝数据库
                
                    //获取工程里，数据库的路径,因为我们已在工程中添加了数据库文件，所以我们要从工程里获取路径
                NSString *backupDbPath = [[NSBundle mainBundle]
                                          pathForResource:@"area"
                                          ofType:@"db"];
                    //这一步实现数据库的添加，
                    // 通过NSFileManager 对象的复制属性，把工程中数据库的路径拼接到应用程序的路径上
                BOOL cp = [fm copyItemAtPath:backupDbPath toPath:_dbPath error:nil];
                NSLog(@"cp = %d",cp);
                    //- (BOOL)copyItemAtPath:(NSString *)srcPath toPath:(NSString *)dstPath error:(NSError **)error
                NSLog(@"backupDbPath =%@",backupDbPath);
                
            }
            
            _db = [FMDatabase databaseWithPath:_dbPath];
            [self selectAddressWithProvice:@"0"];//获取省
            [self selectAddressWithCity:@"1"];//默认获取北京市
            [self selectAddressWithCounty:@"35"];//默认获取北京市区县
        }
        
        
    }
    return self;
}

- (void)selectAddressWithProvice:(NSString *)provice
{
    [_db open];
    FMResultSet *rs = [_db executeQuery:@"select * from area where area_parent=?",provice];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    while ([rs next]) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setValue:[rs stringForColumn:@"area_name"] forKey:@"area_name"];
        [dic setValue:[rs stringForColumn:@"area_parent"] forKey:@"area_parent"];
            //省没有地区编码Code 不存在时，dic 无key:area_code 
        [dic setValue:[rs stringForColumn:@"area_code"] forKey:@"area_code"];
        [dic setValue:[rs stringForColumn:@"area_id"] forKey:@"area_id"];
        
        [array addObject:dic];
    }
    self.pickerArray = array;
    [_db close];
    
}

- (void)selectAddressWithCity:(NSString *)city
{
    [_db open];
    FMResultSet *rs = [_db executeQuery:@"select * from area where area_parent=?",city];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    while ([rs next]) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setValue:[rs stringForColumn:@"area_name"] forKey:@"area_name"];
        [dic setValue:[rs stringForColumn:@"area_parent"] forKey:@"area_parent"];
        [dic setValue:[rs stringForColumn:@"area_code"] forKey:@"area_code"];
        [dic setValue:[rs stringForColumn:@"area_id"] forKey:@"area_id"];
        
            //        NSLog(@"%@",[rs stringForColumn:@"area_name"]);
            //        NSLog(@"%@",[rs stringForColumn:@"area_parent"]);
            //        NSLog(@"%@",[rs stringForColumn:@"area_id"]);
        
        [array addObject:dic];
    }
    self.subPickerArray = array;
    [_db close];
    
}

- (void)selectAddressWithCounty:(NSString *)county
{
    [_db open];
    FMResultSet *rs = [_db executeQuery:@"select * from area where area_parent=?",county];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    while ([rs next]) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setValue:[rs stringForColumn:@"area_name"] forKey:@"area_name"];
        [dic setValue:[rs stringForColumn:@"area_parent"] forKey:@"area_parent"];
        [dic setValue:[rs stringForColumn:@"area_code"] forKey:@"area_code"];
        [dic setValue:[rs stringForColumn:@"area_id"] forKey:@"area_id"];
        
            //        NSLog(@"%@",[rs stringForColumn:@"area_name"]);
            //        NSLog(@"%@",[rs stringForColumn:@"area_parent"]);
            //        NSLog(@"%@",[rs stringForColumn:@"area_id"]);
        
        [array addObject:dic];
    }
    self.thirdPickerArray = array;
    [_db close];
    
}

#pragma mark --
#pragma mark--UIPickerViewDataSource
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component==FirstComponent) {
        return [self.pickerArray count];
    }
    if (component==SubComponent) {
        return [self.subPickerArray count];
    }
    if (component==ThirdComponent) {
        return [self.thirdPickerArray count];
    }
    return 0;
}


#pragma mark--
#pragma mark--UIPickerViewDelegate
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component==FirstComponent) {
        return [self.pickerArray objectAtIndex:row][@"area_name"];
    }
    if (component==SubComponent) {
        return [self.subPickerArray objectAtIndex:row][@"area_name"];
    }
    if (component==ThirdComponent) {
        return [self.thirdPickerArray objectAtIndex:row][@"area_name"];
    }
    return nil;
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSLog(@"row is %d,Component is %d",row,component);
    if (component == 0) {
        if ([self.pickerArray count]>0) {
            [self selectAddressWithCity:[self.pickerArray objectAtIndex:row][@"area_id"]];
        }else{
            self.subPickerArray=nil;
        }
        if ([self.subPickerArray count]>0) {
            [self selectAddressWithCounty:[self.subPickerArray objectAtIndex:0][@"area_id"]];
        }else{
            self.thirdPickerArray=nil;
        }
        [pickerView selectedRowInComponent:1];
        [pickerView reloadComponent:1];
        [pickerView selectedRowInComponent:2];
        
        _count1 = row;
        _count2 = 0;
        _count3 = 0;
    }
    if (component == 1) {
        if ([_pickerArray count]>0&&[_subPickerArray count]>0) {
            [self selectAddressWithCounty:[self.subPickerArray objectAtIndex:row][@"area_id"]];
        }else{
            self.thirdPickerArray=nil;
        }
        [pickerView selectRow:0 inComponent:2 animated:YES];
        
        _count2 = row;
        _count3 = 0;
    }
    if (component == 2) {
        _count3 = row;
    }
    [pickerView reloadComponent:2];
    _handleActionWithAddressView(self.pickerArray[_count1],
                                 self.subPickerArray[_count2],
                                 self.thirdPickerArray[_count3]);
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (component==FirstComponent) {
        return 90.0;
    }
    if (component==SubComponent) {
        return 120.0;
    }
    if (component==ThirdComponent) {
        return 100.0;
    }
    return 0;
}


@end
