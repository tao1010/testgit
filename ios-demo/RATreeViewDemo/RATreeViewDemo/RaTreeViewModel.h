//
//  RaTreeViewModel.h
//  RATreeViewDemo
//
//  Created by dengtao on 2018/8/8.
//  Copyright © 2018年 hehe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RaTreeViewModel : NSObject

@property (nonatomic,copy) NSString *name;//标题

@property (nonatomic,strong) NSArray *children;//子节点数组


    //初始化一个model
- (id)initWithName:(NSString *)name children:(NSArray *)array;

    //遍历构造器
+ (id)dataObjectWithName:(NSString *)name children:(NSArray *)children;


@end
