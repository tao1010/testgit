//
//  RATreeViewCell.m
//  RATreeViewDemo
//
//  Created by dengtao on 2018/8/8.
//  Copyright © 2018年 hehe. All rights reserved.
//

#import "RATreeViewCell.h"

@implementation RATreeViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _titleLable = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, 200, 22)];
        _titleLable.font = [UIFont systemFontOfSize:14];
        _titleLable.textColor = [UIColor darkTextColor];
        [self.contentView addSubview:_titleLable];
        
        
        
        _iconView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width - 50, 0, 44, 44)];
//        _iconView.backgroundColor = [UIColor redColor];
        [self.contentView addSubview:_iconView];
        
    }
    return self;
}


- (void)setCellBasicInfoWith:(NSString *)title level:(NSInteger)level children:(NSInteger )children{
    
        //有自孩子时显示图标
    if (children==0) {
        self.iconView.hidden = YES;
        
    }
    else { //否则不显示
        self.iconView.hidden = NO;
    }
    
    self.titleLable.text = title;
    self.iconView.image = [UIImage imageNamed:@"close"];
    
    
        //每一层的布局
    CGFloat left = 10+level*30;
    
        //头像的位置
    CGRect  iconViewFrame = self.iconView.frame;
    
    iconViewFrame.origin.x = left;
    
    self.iconView.frame = iconViewFrame;
    
        //title的位置
    CGRect titleFrame = self.titleLable.frame;
    
    titleFrame.origin.x = 40+left;
    
    self.titleLable.frame = titleFrame;
    
    
    
}


@end


