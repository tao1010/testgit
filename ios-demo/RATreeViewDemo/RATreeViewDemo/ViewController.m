//
//  ViewController.m
//  RATreeViewDemo
//
//  Created by dengtao on 2018/8/8.
//  Copyright © 2018年 hehe. All rights reserved.
//

#import "ViewController.h"
#import "RATreeView.h"
#import "RATreeViewCell.h"
#import "RaTreeViewModel.h"

@interface ViewController ()<UITableViewDataSource,UITableViewDelegate,RATreeViewDelegate,RATreeViewDataSource>

@property(nonatomic, strong) NSMutableArray *dataArray;
@property(nonatomic, strong) UITableView    *tableView;
@property(nonatomic, strong) RATreeView     *treeView;

@property(nonatomic, strong) NSMutableArray *modelArray;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CGRect rect = CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.height);
    self.tableView = [[UITableView alloc] initWithFrame:rect style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = 44;
//    [self.view addSubview:self.tableView];
    
    self.treeView = [[RATreeView alloc] initWithFrame:rect style:RATreeViewStyleGrouped];
    self.treeView.delegate = self;
    self.treeView.dataSource = self;
    [self.view addSubview:self.treeView];
    [self setData];
}

    //加载数据
- (void)setData{
    
    self.modelArray = [NSMutableArray new];
        //宝鸡市 (四层)
    RaTreeViewModel *zijingcun = [RaTreeViewModel dataObjectWithName:@"紫荆村" children:nil];
    
    RaTreeViewModel *chengcunzheng = [RaTreeViewModel dataObjectWithName:@"陈村镇" children:@[zijingcun]];
    
    RaTreeViewModel *fengxiang = [RaTreeViewModel dataObjectWithName:@"凤翔县" children:@[chengcunzheng]];
    RaTreeViewModel *qishan = [RaTreeViewModel dataObjectWithName:@"岐山县" children:nil];
    RaTreeViewModel *baoji = [RaTreeViewModel dataObjectWithName:@"宝鸡市" children:@[fengxiang,qishan]];
    
        //西安市
    RaTreeViewModel *yantaqu = [RaTreeViewModel dataObjectWithName:@"雁塔区" children:nil];
    RaTreeViewModel *xinchengqu = [RaTreeViewModel dataObjectWithName:@"新城区" children:nil];
    
    RaTreeViewModel *xian = [RaTreeViewModel dataObjectWithName:@"西安" children:@[yantaqu,xinchengqu]];
    
    RaTreeViewModel *shanxi = [RaTreeViewModel dataObjectWithName:@"陕西" children:@[baoji,xian]];
    
    [self.modelArray addObject:shanxi];
    [self.treeView reloadData];
}


//RATreeViewDelegate,RATreeViewDataSource
    //返回行高
- (CGFloat)treeView:(RATreeView *)treeView heightForRowForItem:(id)item {
    
    return 50;
}

    //将要展开
- (void)treeView:(RATreeView *)treeView willExpandRowForItem:(id)item {
    
    RATreeViewCell *cell = (RATreeViewCell *)[treeView cellForItem:item];
    cell.iconView.image = [UIImage imageNamed:@"open"];
    
}
    //将要收缩
- (void)treeView:(RATreeView *)treeView willCollapseRowForItem:(id)item {
    
    RATreeViewCell *cell = (RATreeViewCell *)[treeView cellForItem:item];
    cell.iconView.image = [UIImage imageNamed:@"close"];
    
}
    //已经展开
- (void)treeView:(RATreeView *)treeView didExpandRowForItem:(id)item {
    
    
    NSLog(@"已经展开了");
}
    //已经收缩
- (void)treeView:(RATreeView *)treeView didCollapseRowForItem:(id)item {
    
    NSLog(@"已经收缩了");
}

    //返回cell
- (UITableViewCell *)treeView:(RATreeView *)treeView cellForItem:(id)item {
    
    static NSString *identifier = @"celll";
    RATreeViewCell *cell = [treeView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        
        cell = [[RATreeViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
        //当前item
    RaTreeViewModel *model = item;
    
        //当前层级
    NSInteger level = [treeView levelForCellForItem:item];
    
        //赋值
    [cell setCellBasicInfoWith:model.name level:level children:model.children.count];
    
    return cell;
    
}

/**
 *  必须实现
 *
 *  @param treeView treeView
 *  @param item    节点对应的item
 *
 *  @return  每一节点对应的个数
 */
- (NSInteger)treeView:(RATreeView *)treeView numberOfChildrenOfItem:(id)item
{
    RaTreeViewModel *model = item;
    
    if (item == nil) {
        
        return self.modelArray.count;
    }
    
    return model.children.count;
}
/**
 *必须实现的dataSource方法
 *
 *  @param treeView treeView
 *  @param index    子节点的索引
 *  @param item     子节点索引对应的item
 *
 *  @return 返回 节点对应的item
 */
- (id)treeView:(RATreeView *)treeView child:(NSInteger)index ofItem:(id)item {
    
    RaTreeViewModel *model = item;
    if (item==nil) {
        
        return self.modelArray[index];
    }
    
    return model.children[index];
}


    //cell的点击方法
- (void)treeView:(RATreeView *)treeView didSelectRowForItem:(id)item {
    
        //获取当前的层
    NSInteger level = [treeView levelForCellForItem:item];
    
        //当前点击的model
    RaTreeViewModel *model = item;
    
    NSLog(@"点击的是第%ld层,name=%@",level,model.name);
    
}

    //单元格是否可以编辑 默认是YES
- (BOOL)treeView:(RATreeView *)treeView canEditRowForItem:(id)item {
    
    return YES;
}

    //编辑要实现的方法
- (void)treeView:(RATreeView *)treeView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowForItem:(id)item {
    
    NSLog(@"编辑了实现的方法");
    
    
}


//UITableViewDataSource,UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.textLabel.text = @"测试";
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
