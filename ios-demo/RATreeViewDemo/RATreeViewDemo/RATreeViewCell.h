//
//  RATreeViewCell.h
//  RATreeViewDemo
//
//  Created by dengtao on 2018/8/8.
//  Copyright © 2018年 hehe. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CellModel;

@interface RATreeViewCell : UITableViewCell

@property (strong, nonatomic)  UIImageView *iconView;//图标
@property (strong, nonatomic)  UILabel *titleLable;//标题

    //赋值
- (void)setCellBasicInfoWith:(NSString *)title level:(NSInteger)level children:(NSInteger )children;


@end


