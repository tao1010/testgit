//
//  RaTreeViewModel.m
//  RATreeViewDemo
//
//  Created by dengtao on 2018/8/8.
//  Copyright © 2018年 hehe. All rights reserved.
//

#import "RaTreeViewModel.h"

@implementation RaTreeViewModel

- (id)initWithName:(NSString *)name children:(NSArray *)children
{
    self = [super init];
    if (self) {
        self.children = children;
        self.name = name;
    }
    return self;
}

+ (id)dataObjectWithName:(NSString *)name children:(NSArray *)children
{
    return [[self alloc] initWithName:name children:children];
}

@end
